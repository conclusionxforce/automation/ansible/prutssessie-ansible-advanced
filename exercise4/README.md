Exercise 4: Configure Loadbalancer
=====================

Needed:
* control node
* web1
* web2
* proxy1

Preparations:
* Exercise 1 completed

Goal:
Build a playbook to install and configure haproxy on proxy1
haproxy should balance traffic over the two webservers
BONUS: write a playbook to test whether all traffic will be 
directed to the other server if one webserver goes down.

Hints:
* The haproxy package is simply called 'haproxy'
* https://cbonte.github.io/haproxy-dconv/1.7/configuration.html

Are you stuck?
* In Lab/README.md the exercise is written out in steps
* In Solutions you can find a working playbook

