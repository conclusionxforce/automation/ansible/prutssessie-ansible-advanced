Exercise 3: Step by step Solution
=================================

* Define a dictionary of the vhosts you want to configure, and their options
  call this 'vhosts'
  You can do this directly in your playbook, or use a separate yml file

* You can use the ansible module 'blockinfile' to add the configuration 
  for each vhost to the apache configuration. Use a new .conf file in 
  /etc/httpd/conf.d/ Example of block:
    <VirtualHost: *:8080>
        ServerName: wordpress.xforce.nl
        DocumentRoot: /var/www/NL/
    </VirtualHost>

* Loop this action using ' loop: "{{ vhosts }}" ' to add all vhosts in the list

* Make sure you use a different marker for each vhost (containing the name
  of the vhost ), so the next blockinfile will not overwrite the 
  previous one

* Create directories and content for each vhost using 'file' and 'copy'
  or if you want to be fancy use a template

* Set the type of the docroot directory (and its contents) to httpd_sys_content_t
  use the modules 'sefcontext' and 'command' or 'shell' (to run restorecon) for this

* Do not forget to restart the httpd service

 

