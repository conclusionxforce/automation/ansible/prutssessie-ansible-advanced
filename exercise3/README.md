Exercise 3: Configure Virtual Hosts
=====================

Needed:
* control node
* web1
* web2

Preparations:
* Exercise 1 completed

Goal:
The webservers should be reachable on 3 different URLs, and only on these.
Each URL should have its own document root
wordpress.xforce.nl   on port 8080   with docroot /var/www/NL/
wordpress.xforce.com  on port 8081   with docroot /var/www/COM/
wordpress.xforce.org  on port 8082   with docroot /var/www/ORG/
Write a playbook to configure apache on the webservers to accomplish this.
The vhosts, ports and docroot should be defined in a separate yml file in
a dictionary

Bonus: include tasks in this playbook to test whether the webservers can 
be reached on these URLs, and not on a random URL, the wrong port or their IP

Hints:
* https://httpd.apache.org/docs/2.4/
* To make a server unreachable, define a default virtual host
* SElinux is enabled on the webservers. you will have to make sure the docroot is reachable

Are you stuck?
* ansible has a nice module for SElinux context setting: sefcontext 
* In Lab/README.md the exercise is written out in steps
* In Solutions you can find a working playbook

