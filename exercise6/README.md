Oefening 1: WordPress
=====================

Benodigd:
* control node
* web1
* web2
* db1

Voorbereiding:
* Je kan met SSH inloggen als centos@$ip van controlnode
* Vervolgens daar met sudo root worden
* Je hebt als root een SSH private/public keypair gegenereerd
* Je hebt de public key verspreid naar de clientnodes, user ansible
* Je kunt als ansible@$clientnode zonder wachtwoord inloggen
* Je kunt vervolgens op de clientnode sudo doen zonder wachtwoord

De webserver en databaseserver zijn wel geinstalleerd, maar nog niet
geconfigureerd. Dit gaan we regelen met Ansible. Installeer Apache, PHP en
Wordpress op de webserver. Wordpress heeft ook een MySQL database nodig op de
databaseserver inclusief bijbehorende toegang, installeer MySQL en maak een
database aan. Bouw voor dit alles een play en zorg dat de credentials in een
vault terecht komen.

Hints:
* Wordpress is te downloaden vanaf https://wordpress.org/latest.tar.gz
* Voor MySQL zijn er heel erg fijne mysql_user en mysql_db modules
* De publieke ip adressen staan op de controlnode in /etc/hosts
* SELinux staat aan. Zorg dat Apache connectie mag maken met MySQL.

Kom je er niet helemaal uit? 
* In Lab/README.md staan stap voor stap aanwijzingen
* In Solutions staan voorbeelden van hoe je dit aan kunt pakken

Uitkomst: WordPress installer beschikbaar op:
* http://$public ip van web1/wordpress/
* http://$public ip van web2/wordpress/
