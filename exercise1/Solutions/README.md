Oplossing
=========

Ik heb voor de duidelijkheid de opdracht opgedeeld over meerdere playbooks.

* Het wachtwoord op vault.yml is 'password'
* apache.yml installeert Apache en PHP packages, zet de service aan en zet de SELinux booleans goed
* mysql.yml installeert MariaDB en bijbehorende packages, zet de service aan en maakt een database en gebruiker aan, en wijst de benodigde rechten toe
* wordpress.yml downloadt het WordPress package, pakt deze uit, en zet een wp-config.php file neer met de juiste databaseconfiguratie, gebaseerd op het template
* mysql.yml en wordpress.yml halen de databasecredentials uit vault.yml
